import {expect} from 'chai'
import {shallowMount} from '@vue/test-utils'
import Success from '@/views/Success.vue'

describe('Success.vue', () => {
  it('shows correct message', () => {
    const message = 'This is the success message'
    const wrapper = shallowMount(Success, {
      propsData: {message}
    })
    expect(wrapper.text()).to.include(message)
  })

  it('contains Success in title', () => {
    const wrapper = shallowMount(Success)
    expect(wrapper.text()).to.include('Success')
  })
})
