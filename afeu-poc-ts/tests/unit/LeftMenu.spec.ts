import {expect} from 'chai'
import {createLocalVue, shallowMount} from '@vue/test-utils'
import ElementUI from 'element-ui'
import LeftMenu from '@/components/navigation/LeftMenu.vue'

describe('LeftMenu.vue', () => {

  const localVue = createLocalVue()
  localVue.use(ElementUI)

  it('Displays correct menus when logged out', () => {
    const wrapper = shallowMount(LeftMenu, {
      localVue,
      computed: {
        isLoggedIn() {
          return false
        }
      }
    })
    const result = wrapper.text()
    expect(result).to.include('Home')
    expect(result).not.to.include('Sozialhilfebezüger')
    expect(result).not.to.include('Arzt')
    expect(result).not.to.include('Bank')
  })

  it('Displays correct menus when logged in', () => {
    const wrapper = shallowMount(LeftMenu, {
      localVue,
      computed: {
        isLoggedIn() {
          return true
        }
      }
    })
    const result = wrapper.text()
    expect(result).to.include('Home')
    expect(result).to.include('Sozialhilfebezüger')
    expect(result).to.include('Arzt')
    expect(result).to.include('Bank')
  })
})
