import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      component: () => import('@/views/Home.vue'),
      meta: {
        title: 'AFEU Upload'
      }
    },
    {
      path: '/bezueger',
      component: () => import('@/views/sozialhilfebezueger/Sozialhilfebezueger.vue'),
      meta: {
        title: 'Sozialhilfebezüger'
      }
    },
    {
      path: '/arzt',
      component: () => import('@/views/arzt/Arzt.vue'),
      meta: {
        title: 'Arzt'
      }
    },
    {
      path: '/bank',
      component: () => import('@/views/bank/Bank.vue'),
      meta: {
        title: 'Bank'
      }
    },
    {
      path: '/success',
      name: 'success',
      props: true,
      component: () => import('@/views/Success.vue'),
      meta: {
        title: 'Erfolg'
      }
    }
  ]
})

router.afterEach((to, from) => {
  document.title = to.meta.title
})

export default router
