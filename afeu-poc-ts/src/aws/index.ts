import aws from 'aws-sdk'
import axios from 'axios'
import moment from 'moment'

// TODO evtl. noch mit .env file ersetzten
// secretAccessKey: process.env.VUE_APP_AWS_SECRET_ACCESS_KEY,
// accessKeyId: process.env.VUE_APP_AWS_ACCESS_KEY,
// region: process.env.VUE_APP_AWS_REGION,

interface S3BucketResponse {
  fileUrl: string
  fileName: string
  fileKey: string
}

aws.config.update({
  secretAccessKey: 'BTKVECBZhwUuARrp1Xt+jE6NP2ZbvGU9D5FZDivR',
  accessKeyId: 'AKIAJCVLD7DQ2RQTUO5Q'
})

export const s3 = new aws.S3({
  signatureVersion: 'v4',
  region: 'eu-central-1'
})

export const singleUpload = async (
  file: any,
  category: string,
  folder?: string
): Promise<S3BucketResponse> => {
  console.log('Beginn with singleUpload')

  // const key = folder + '/' + Date.now() + '-' + file.name.replace(/\s/g, '-')
  const fileExtension = file.name.substr(file.name.lastIndexOf('.'), file.name.length)
  const dateString = moment().format('YYYYMMDD')
  const fileName = category + '_' + dateString
  const fileKey = fileName + fileExtension

  const params = {
    // Bucket: process.env.VUE_APP_AWS_BUCKET,
    Bucket: 'node-afeu',
    Key: fileKey,
    ContentType: file.type
  }

  const url = s3.getSignedUrl('putObject', params)

  const result = await axios.put(url, file, {
    headers: {
      'Content-Type': file.type
    }
  })

  if (result.status === 200) {
    const bucketUrl = decodeURIComponent(result.request.responseURL).split(fileKey)[0]
    const fileUrl = bucketUrl + fileKey

    return {fileUrl, fileName, fileKey}
  }
  throw new Error('Could not upload file')
}

export const deleteObjectByKey = (fileKey) => {
  const params = {
    // Bucket: process.env.VUE_APP_AWS_BUCKET,
    Bucket: 'node-afeu',
    Key: fileKey
  }
  const data = s3.deleteObject(params).promise()

  return data
}

export const createUrl = (fileName) => {
  const params = {
    Bucket: 'node-afeu',
    Key: fileName,
    Expires: 604800
  }

  const url = s3.getSignedUrl('getObject', params)

  return url
}
