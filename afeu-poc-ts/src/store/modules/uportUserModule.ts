import {VuexModule, Module, Mutation, Action, getModule} from 'vuex-module-decorators'
import store from '../../store'
import {UportUser} from '../../models'

export interface IUportUserState {
  user: UportUser
  isloggedIn: boolean
}

@Module({dynamic: true, store, name: 'uportUser'})
class UserModule extends VuexModule implements IUportUserState {
  public user: UportUser = {did: '', name: ''}
  public isloggedIn = false

  @Mutation
  setUser(user: UportUser) {
    this.user = user
    this.isloggedIn = true
  }

  @Mutation
  logout() {
    this.user = {did: '', name: ''}
    this.isloggedIn = false
  }

  @Mutation
  setIsLoggedIn(loggedIn: boolean) {
    this.isloggedIn = loggedIn
  }
}

export const UportUserModule = getModule(UserModule)
