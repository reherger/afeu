import {Credentials} from 'uport-credentials'

// created identity with
// const identity = Credentials.createIdentity()

const AppCredentials = new Credentials({
  appName: 'AFEU',
  did: 'did:ethr:0x70285c1a4b3bcae0b11451c737ce54f5440754fd',
  privateKey: 'd7ff57d5032f6a6f7074cb1f40b7b80983ce79c3e5e6e35fcbc70d0a928677a6'
})

export default AppCredentials
