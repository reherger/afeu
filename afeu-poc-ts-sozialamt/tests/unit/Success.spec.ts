import {expect} from 'chai'
import {createLocalVue, shallowMount} from '@vue/test-utils'
import ElementUI from 'element-ui'
import Success from '@/views/Success.vue'

describe('Success.vue', () => {

  const localVue = createLocalVue()
  localVue.use(ElementUI)

  it('renders correct success message when a claim value is present', () => {
    const claimValue = 'has a value'
    const category = 'testCategory'
    const date = '2019-06-03'
    const wrapper = shallowMount(Success, {
      localVue,
      propsData: {claimValue, category, date}
    })
    const result = wrapper.text()
    expect(result).to.include('Claim der Kategorie')
    expect(result).to.include(category)
    expect(result).to.include(date)
  })

  it('do not render anything it no value is present', () => {
    const claimValue = null
    const category = 'testCategory'
    const date = '2019-06-03'
    const wrapper = shallowMount(Success, {
      localVue,
      propsData: {claimValue, category, date}
    })
    const result = wrapper.text()
    expect(result).not.to.include('Claim der Kategorie')
    expect(result).not.to.include(category)
    expect(result).not.to.include(date)
  })
})
