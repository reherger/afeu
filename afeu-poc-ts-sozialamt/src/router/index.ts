import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      component: () => import('@/views/Home.vue'),
      meta: {
        title: 'AFEU Portal'
      }
    },
    {
      path: '/register',
      component: () => import('@/views/Register.vue'),
      meta: {
        title: 'Register'
      }
    },
    {
      path: '/share',
      component: () => import('@/views/Share.vue'),
      meta: {
        title: 'Share'
      }
    },
    {
      path: '/account',
      component: () => import('@/views/Account.vue'),
      meta: {
        title: 'Account'
      }
    },
    {
      path: '/success',
      name: 'success',
      props: true,
      component: () => import('@/views/Success.vue'),
      meta: {
        title: 'Erfolg'
      }
    },
    {
      path: '/registersuccess',
      name: 'registersuccess',
      component: () => import('@/views/RegisterSuccess.vue'),
      meta: {
        title: 'Erfolg'
      }
    }
  ]
})

router.afterEach((to, from) => {
  document.title = to.meta.title
})

export default router
