export class UportUser {
  did: string
  name: string

  constructor(did: string, name: string) {
    this.did = did
    this.name = name
  }
}

export interface Registration {
  salutation: string
  firstname: string
  lastname: string
  civilStatus: string
  birthday: Date | undefined
  socialNo: string
  street: string
  zip: string
  city: string
  email: string
  telPrivate: string
  telMobile: string
  withoutJob: Date | undefined
  employer: string
  salary: Date | undefined
}
