import {VuexModule, Module, Mutation, Action, getModule} from 'vuex-module-decorators'
import store from '..'

export interface IErrorState {
  errors: string[]
}

@Module({dynamic: true, store, name: 'errors'})
class ErrorModule extends VuexModule implements IErrorState {
  public errors: string[] = []

  @Mutation
  addError(error: string) {
    this.errors.push(error)
  }

  @Mutation
  removeError(index: number) {
    this.errors.splice(index, 1)
  }

  @Mutation
  clearAllErrors() {
    this.errors = []
  }

  get hasErrors() {
    return this.errors.length > 0
  }
}

export const ErrorsModule = getModule(ErrorModule)
