import {VuexModule, Module, Mutation, Action, getModule} from 'vuex-module-decorators'
import store from '../../store'
import {UportUser, Registration} from '../../models'

export interface IUportUserState {
  user: UportUser
  registration: Registration | undefined
  isloggedIn: boolean
}

@Module({dynamic: true, store, name: 'uportUser'})
class UserModule extends VuexModule implements IUportUserState {
  public user: UportUser = {did: '', name: ''}
  public registration: Registration | undefined = undefined
  public isloggedIn = false

  @Mutation
  setUser(user: UportUser) {
    this.user = user
    this.isloggedIn = true
  }

  @Mutation
  setRegistration(registration: Registration) {
    this.registration = registration
  }

  @Mutation
  logout() {
    this.user = {did: '', name: ''}
    this.isloggedIn = false
    this.registration = undefined
  }

  @Mutation
  setIsLoggedIn(loggedIn: boolean) {
    this.isloggedIn = loggedIn
  }

  get registrationData() {
    return this.registration
  }
}

export const UportUserModule = getModule(UserModule)
