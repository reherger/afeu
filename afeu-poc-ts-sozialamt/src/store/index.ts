import Vue from 'vue'
import Vuex from 'vuex'
import {IUportUserState} from './modules/uportUserModule'

Vue.use(Vuex)

export interface IRootState {
  uportUser: IUportUserState
}

// Declare empty store first, dynamically register all modules later.
export default new Vuex.Store<IRootState>({})
