# uport-demo
- Download uPort App

- Node Js installieren (v10.15.3 LTS)

- Git installieren

- Repo clonen: git clone https://USERNAME@bitbucket.org/reherger/afeu.git
  USERNAME mit Bitbucket user ersetzten

- Execute 'npm install' from console within this folder (Uport-Demo)

- Execute 'node index.js' or 'npm run start' to run the local server that is published with ngrok

- Server startet und ist über ngrok erreichbar (Publish local server to internet)

- Server generiert QR Code mit uPort request

- User scannt den QR Code mit uPort app

- Server erhält response und verifiziert diese

- Server schickt eine Attestation per Push Notification an den User